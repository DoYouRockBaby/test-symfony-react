AIREPLAY-NG(8)                                                                                                                                                       System Manager's Manual                                                                                                                                                      AIREPLAY-NG(8)

NNAAMMEE
       aireplay-ng - inject packets into a wireless network to generate traffic

SSYYNNOOPPSSIISS
       aaiirreeppllaayy--nngg [options] <replay interface>

DDEESSCCRRIIPPTTIIOONN
       aaiirreeppllaayy--nngg  is  used  to  inject/replay  frames.   The  primary function is to generate traffic for the later use in aircrack-ng for cracking the WEP and WPA-PSK keys. There are different attacks which can cause deauthentications for the purpose of capturing WPA handshake data, fake authentications, Interactive packet replay, hand-crafted ARP
       request injection and ARP-request reinjection. With the packetforge-ng tool it's possible to create arbitrary frames.

       aaiirreeppllaayy--nngg supports single-NIC injection/monitor.

       This feature needs driver patching.

OOPPTTIIOONNSS
       _-_H_, _-_-_h_e_l_p
              Shows the help screen.

       FFiilltteerr ooppttiioonnss::

       _-_b _<_b_s_s_i_d_>
              MAC address of access point.

       _-_d _<_d_m_a_c_>
              MAC address of destination.

       _-_s _<_s_m_a_c_>
              MAC address of source.

       _-_m _<_l_e_n_>
              Minimum packet length.

       _-_n _<_l_e_n_>
              Maximum packet length.

       _-_u _<_t_y_p_e_>
              Frame control, type field.

       _-_v _<_s_u_b_t_>
              Frame control, subtype field.

       _-_t _<_t_o_d_s_>
              Frame control, "To" DS bit (0 or 1).

       _-_f _<_f_r_o_m_d_s_>
              Frame control, "From" DS bit (0 or 1).

       _-_w _<_i_s_w_e_p_>
              Frame control, WEP bit (0 or 1).

       _-_D     Disable AP Detection.

       RReeppllaayy ooppttiioonnss::

       _-_x _<_n_b_p_p_s_>
              Number of packets per second.

       _-_p _<_f_c_t_r_l_>
              Set frame control word (hex).

       _-_a _<_b_s_s_i_d_>
              Set Access Point MAC address.

       _-_c _<_d_m_a_c_>
              Set destination MAC address.

       _-_h _<_s_m_a_c_>
              Set source MAC address.

       _-_g _<_n_b___p_a_c_k_e_t_s_>
              Change ring buffer size (default: 8 packets). The minimum is 1.

       _-_F     Choose first matching packet.

       _-_e _<_e_s_s_i_d_>
              Fake Authentication attack: Set target SSID (see below). For SSID containing special characters, see https://www.aircrack-ng.org/doku.php?id=faq#how_to_use_spaces_double_quote_and_single_quote_etc_in_ap_names

       _-_o _<_n_p_a_c_k_e_t_s_>
              Fake Authentication attack: Set the number of packets for every authentication and association attempt (Default: 1). 0 means auto

       _-_q _<_s_e_c_o_n_d_s_>
              Fake Authentication attack: Set the time between keep-alive packets in fake authentication mode.

       _-_Q     Fake Authentication attack: Sends reassociation requests instead of performing a complete authentication and association after each delay period.

       _-_y _<_p_r_g_a_>
              Fake Authentication attack: Specifies the keystream file for fake shared key authentication.

       _-_T _n   Fake Authentication attack: Exit if fake authentication fails 'n' time(s).

       _-_j     ARP Replay attack : inject FromDS packets (see below).

       _-_k _<_I_P_>
              Fragmentation attack: Set destination IP in fragments.

       _-_l _<_I_P_>
              Fragmentation attack: Set source IP in fragments.

       _-_B     Test option: bitrate test.

       SSoouurrccee ooppttiioonnss::

       _-_i _<_i_f_a_c_e_>
              Capture packets from this interface.

       _-_r _<_f_i_l_e_>
              Extract packets from this pcap file.

       MMiisscceellllaanneeoouuss ooppttiioonnss::

       _-_R     disable /dev/rtc usage.

       _-_-_i_g_n_o_r_e_-_n_e_g_a_t_i_v_e_-_o_n_e if the interface's channel can't be determined ignore the mismatch, needed for unpatched cfg80211

       _-_-_d_e_a_u_t_h_-_r_c _<_r_c_>_, _-_Z _<_r_c_> Provide a reason code when doing deauthication (between 0 and 255). By default, 7 is used: Class 3 frame received from unassociated STA. 0 is a reserved value. Reason codes explanations can be found in the IEEE802.11 standard or in https://mrncciew.com/2014/10/11/802-11-mgmt-deauth-disassociation-frames/

       AAttttaacckk mmooddeess::

       _-_0 _<_c_o_u_n_t_>_, _-_-_d_e_a_u_t_h_=_<_c_o_u_n_t_>
              This attack sends deauthentication packets to one or more clients which are currently associated with a particular access point. Deauthenticating clients can be done for a number of reasons: Recovering a hidden ESSID. This is an ESSID which is not being broadcast. Another term for this is "cloaked" or Capturing  WPA/WPA2  handshakes  by
              forcing clients to reauthenticate or Generate ARP requests (Windows clients sometimes flush their ARP cache when disconnected).  Of course, this attack is totally useless if there are no associated wireless client or on fake authentications.

       _-_1 _<_d_e_l_a_y_>_, _-_-_f_a_k_e_a_u_t_h_=_<_d_e_l_a_y_>
              The  fake authentication attack allows you to perform the two types of WEP authentication (Open System and Shared Key) plus associate with the access point (AP). This is only useful when you need an associated MAC address in various aireplay-ng attacks and there is currently no associated client. It should be noted that the fake authen‐
              tication attack does NOT generate any ARP packets. Fake authentication cannot be used to authenticate/associate with WPA/WPA2 Access Points.

       _-_2_, _-_-_i_n_t_e_r_a_c_t_i_v_e
              This attack allows you to choose a specific packet for replaying (injecting). The attack can obtain packets to replay from two sources. The first being a live flow of packets from your wireless card. The second being from a pcap file. Reading from a file is an often overlooked feature of aireplay-ng. This allows you  read  packets  from
              other capture sessions or quite often, various attacks generate pcap files for easy reuse. A common use of reading a file containing a packet your created with packetforge-ng.

       _-_3_, _-_-_a_r_p_r_e_p_l_a_y
              The  classic  ARP  request  replay attack is the most effective way to generate new initialization vectors (IVs), and works very reliably. The program listens for an ARP packet then retransmits it back to the access point. This, in turn, causes the access point to repeat the ARP packet with a new IV. The program retransmits the same ARP
              packet over and over. However, each ARP packet repeated by the access point has a new IVs. It is all these new IVs which allow you to determine the WEP key.

       _-_4_, _-_-_c_h_o_p_c_h_o_p
              This attack, when successful, can decrypt a WEP data packet without knowing the key. It can even work against dynamic WEP. This attack does not recover the WEP key itself, but merely reveals the plaintext. However, some access points are not vulnerable to this attack. Some may seem vulnerable at first  but  actually  drop  data  packets
              shorter  that  60  bytes. If the access point drops packets shorter than 42 bytes, aireplay tries to guess the rest of the missing data, as far as the headers are predictable. If an IP packet is captured, it additionally checks if the checksum of the header is correct after guessing the missing parts of it. This attack requires at least
              one WEP data packet.

       _-_5_, _-_-_f_r_a_g_m_e_n_t
              This attack, when successful, can obtain 1500 bytes of PRGA (pseudo random generation algorithm). This attack does not recover the WEP key itself, but merely obtains the PRGA. The PRGA can then be used to generate packets with packetforge-ng which are in turn used for various injection attacks. It requires at least one data packet to be
              received from the access point in order to initiate the attack.

       _-_6_, _-_-_c_a_f_f_e_-_l_a_t_t_e
              In general, for an attack to work, the attacker has to be in the range of an AP and a connected client (fake or real). Caffe Latte attacks allows one to gather enough packets to crack a WEP key without the need of an AP, it just need a client to be in range.

       _-_7_, _-_-_c_f_r_a_g
              This attack turns IP or ARP packets from a client into ARP request against the client. This attack works especially well against ad-hoc networks. As well it can be used against softAP clients and normal AP clients.

       _-_8_, _-_-_m_i_g_m_o_d_e
              This  attack  works against Cisco Aironet access points configured in WPA Migration Mode, which enables both WPA and WEP clients to associate to an access point using the same Service Set Identifier (SSID).  The program listens for a WEP-encapsulated broadcast ARP packet, bitflips it to make it into an ARP coming from the attacker's MAC
              address and retransmits it to the access point. This, in turn, causes the access point to repeat the ARP packet with a new IV and also to forward the ARP reply to the attacker with a new IV. The program retransmits the same ARP packet over and over. However, each ARP packet repeated by the access point has a new IV as does the ARP reply
              forwarded to the attacker by the access point. It is all these new IVs which allow you to determine the WEP key.

       _-_9_, _-_-_t_e_s_t
              Tests injection and quality.

FFRRAAGGMMEENNTTAATTIIOONN VVEERRSSUUSS CCHHOOPPCCHHOOPP
       FFrraaggmmeennttaattiioonn::

              _P_r_o_s
              - Can obtain the full packet length of 1500 bytes XOR. This means you can subsequently pretty well create any size of packet.
              - May work where chopchop does not
              - Is extremely fast. It yields the XOR stream extremely quickly when successful.

              _C_o_n_s
              - Setup to execute the attack is more subject to the device drivers. For example, Atheros does not generate the correct packets unless the wireless card is set to the mac address you are spoofing.
              - You need to be physically closer to the access point since if any packets are lost then the attack fails.

       CChhooppcchhoopp

              _P_r_o
              - May work where frag does not work.

              _C_o_n_s
              - Cannot be used against every access point.
              - The maximum XOR bits is limited to the length of the packet you chopchop against.
              - Much slower then the fragmentation attack.

AAUUTTHHOORR
       This  manual  page was written by Adam Cecile <gandalf@le-vert.net> for the Debian system (but may be used by others).  Permission is granted to copy, distribute and/or modify this document under the terms of the GNU General Public License, Version 2 or any later version published by the Free Software Foundation On Debian systems, the complete
       text of the GNU General Public License can be found in /usr/share/common-licenses/GPL.

SSEEEE AALLSSOO
       aaiirrbbaassee--nngg((88))
       aaiirrmmoonn--nngg((88))
       aaiirroodduummpp--nngg((88))
       aaiirroodduummpp--nngg--oouuii--uuppddaattee((88))
       aaiirrsseerrvv--nngg((88))
       aaiirrttuunn--nngg((88))
       bbeessssiiddee--nngg((88))
       eeaassssiiddee--nngg((88))
       ttkkiippttuunn--nngg((88))
       wweessssiiddee--nngg((88))
       aaiirrccrraacckk--nngg((11))
       aaiirrddeeccaapp--nngg((11))
       aaiirrddeeccllooaakk--nngg((11))
       aaiirroolliibb--nngg((11))
       bbeessssiiddee--nngg--ccrraawwlleerr((11))
       bbuuddddyy--nngg((11))
       iivvssttoooollss((11))
       kkssttaattss((11))
       mmaakkeeiivvss--nngg((11))
       ppaacckkeettffoorrggee--nngg((11))
       wwppaacclleeaann((11))
       aaiirrvveennttrriillooqquuiisstt((88))

Version 1.5.2                                                                                                                                                             December 2018                                                                                                                                                           AIREPLAY-NG(8)
