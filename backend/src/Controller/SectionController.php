<?php

namespace App\Controller;

use App\Entity\Area;
use App\Entity\Section;
use App\Entity\User;
use App\Entity\LoginRequest;
use App\Repository\SectionRepository;
use App\Security\UsernamePasswordAuthenticator;
use App\Utility\EntityUtility;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class SectionController extends AbstractController
{
    /** @var EntityUtility */
    private $entityUtility;

    /** @var SectionRepository */
    private $sectionRepository;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var TranslatorInterface */
    private $translator;

    /**
     * Constructor, use dependency injection to grab depedencies
     *
     * @param EntityUtility $entityUtility
     * @param SectionRepository $sectionRepository
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityUtility $entityUtility, SectionRepository $sectionRepository, EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->entityUtility = $entityUtility;
        $this->sectionRepository = $sectionRepository;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * Get section informations
     * 
     * @param Area $area of the section to display
     * @param Section $section to display
     * 
     * @View(serializerGroups={"section"}, populateDefaultVars=false)
     * @Rest\Get("/area/{area_id}/section/{section_id}", name="section-get")
     * @ParamConverter("area", options={"id" = "area_id"})
     * @ParamConverter("section", options={"id" = "section_id"})
     */
    public function find(Area $area, Section $section)
    {
        //Check if the section is located into the area
        if($section->getArea()->getId() != $area->getId())
        {
            return new JsonResponse(array('message' => 'This section is not located into this area'), Response::HTTP_NOT_FOUND);
        }

        //Check if the logged user is an user for the section
        if(empty($this->getUser()) || !$section->getArea()->getUsers()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to see this section'), Response::HTTP_NOT_FOUND);
        }

        return $section;
    }

    /**
     * Create a new section
     * 
     * @param Area $area of the section to create
     * @param Section $section to create
     * 
     * @View(serializerGroups={"section"}, populateDefaultVars=false)
     * @Rest\Post("/area/{id}/section", name="section-create")
     * @ParamConverter("section", converter="fos_rest.request_body")
     */
    public function create(Area $area, Section $section)
    {
        $errors = [];

        $section->setArea($area);

        if($this->entityUtility->validate($section, $errors))
        {
            //Check if the logged user is an admin for the section
            if(empty($this->getUser()) || !$section->getArea()->getAdmins()->exists(function($key, User $value) {
                return $value->getId() === $this->getUser()->getId();
            }))
            {
                return new JsonResponse(array('message' => 'You\'r not allowed to create this section'), Response::HTTP_FORBIDDEN);
            }

            //check if the section title aldready exists
            if($this->sectionRepository->findOneByTitle($section->getTitle()))
            {
                return new JsonResponse(array([
                    'property' => 'title',
                    'message' => 'This title aldready exists',
                ]), 400);
            }

            //Insert the section in the database
            $this->entityManager->persist($section);
            $this->entityManager->flush();

            return $section;
        }
        else
        {
            return new JsonResponse($errors, 400);
        }
    }

    /**
     * Update section informations
     * 
     * @param int $areaId of the area of the section to update
     * @param int $id of the section to update
     * @param Request $request the request
     * 
     * @View(serializerGroups={"section"}, populateDefaultVars=false)
     * @Rest\Put("/area/{areaId}/section/{id}", name="section-update")
     */
    public function update(int $areaId, int $id, Request $request)
    {
        ///Find section
        $section = $this->sectionRepository->find($id);

        //Check if the section exists
        if(!$section)
        {
            return new JsonResponse(array('message' => 'Section not found'), Response::HTTP_NOT_FOUND);
        }

        //Check if the section is located into the area
        if($section->getArea()->getId() != $areaId)
        {
            return new JsonResponse(array('message' => 'This section is not located into this area'), Response::HTTP_NOT_FOUND);
        }

        //Check if the logged user is an admin for the section
        if(empty($this->getUser()) || !$section->getArea()->getAdmins()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to modify this section'), Response::HTTP_NOT_FOUND);
        }

        //Parse request body
        $body = json_decode($request->getContent(), true);
        if(empty($body) || !is_array($body)) {
            return new JsonResponse(array('message' => 'Incorrect body format'), Response::HTTP_BAD_REQUEST);
        }

        //If title request is not empty, we change it
        if(!empty($body['title']))
        {
            $section->setTitle($body['title']);
        }

        $errors = [];
        if($this->entityUtility->validate($section, $errors))
        {
            //Save the changes in the database
            $this->entityManager->flush();
            return $section;
        }
        else
        {
            return new JsonResponse($errors, 400);
        }
    }

    /**
     * Delete an section
     * 
     * @param int $areaId of the area of the section to delete
     * @param int $id of the section to delete
     * 
     * @View(serializerGroups={"section"}, populateDefaultVars=false)
     * @Rest\Delete("/area/{areaId}/section/{id}", name="section-delete")
     */
    public function delete(int $areaId, int $id)
    {
        ///Find section
        $section = $this->sectionRepository->find($id);

        //Check if section exists
        if(!$section)
        {
            return new JsonResponse(array('message' => 'Section not found'), Response::HTTP_NOT_FOUND);
        }

        //Check if the section is located into the area
        if($section->getArea()->getId() != $areaId)
        {
            return new JsonResponse(array('message' => 'This section is not located into this area'), Response::HTTP_NOT_FOUND);
        }

        //Check if the logged user is an admin for the section
        if(empty($this->getUser()) || !$section->getArea()->getAdmins()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to delete this section'), Response::HTTP_NOT_FOUND);
        }

        //Delete the section
        $this->entityManager->remove($section);
        $this->entityManager->flush();

        return true;
    }
}
