<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\LoginRequest;
use App\Repository\UserRepository;
use App\Repository\TokenRepository;
use App\Security\UsernamePasswordAuthenticator;
use App\Utility\EntityUtility;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class UserController extends AbstractController
{
    /** @var EntityUtility */
    private $entityUtility;

    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;

    /** @var UserRepository */
    private $userRepository;

    /** @var TokenRepository */
    private $tokenRepository;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var TranslatorInterface */
    private $translator;

    /**
     * Constructor, use dependency injection to grab depedencies
     *
     * @param EntityUtility $entityUtility
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityUtility $entityUtility, UserPasswordEncoderInterface $passwordEncoder, UserRepository $userRepository, TokenRepository $tokenRepository, EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->entityUtility = $entityUtility;
        $this->passwordEncoder = $passwordEncoder;
        $this->userRepository = $userRepository;
        $this->tokenRepository = $tokenRepository;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * Search among users
     * 
     * @param string $searchString
     * 
     * @View(serializerGroups={"summary"}, populateDefaultVars=false)
     * @Rest\Get("/user/search/{searchString}", name="user-search")
     */
    public function search(string $searchString)
    {
        return $this->userRepository->search($searchString);
    }

    /**
     * Get user informations
     * 
     * @param User $user to display
     * 
     * @View(serializerGroups={"user"}, populateDefaultVars=false)
     * @Rest\Get("/user/{username}", name="user-get")
     */
    public function find(User $user)
    {
        return $user;
    }

    /**
     * Register a new user
     * 
     * @param User $user to register
     * 
     * @View(serializerGroups={"user"}, populateDefaultVars=false)
     * @Rest\Post("/user", name="user-create")
     * @ParamConverter("user", converter="fos_rest.request_body")
     */
    public function create(User $user)
    {
        $errors = [];
        
        if($this->entityUtility->validate($user, $errors))
        {
            //check if the user aldready exists
            if($this->userRepository->findOneByUsername($user->getUsername()))
            {
                return new JsonResponse(array([
                    'property' => 'username',
                    'message' => 'This username aldready exists',
                ]), 400);
            }
            else
            {
                //Encrypt the password
                $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));

                //Insert the user in the database
                $this->entityManager->persist($user);
                $this->entityManager->flush();

                return $user;
            }
        }
        else
        {
            return new JsonResponse($errors, 400);
        }
    }

    /**
     * Update user informations
     * 
     * @param string $username of the user to update
     * @param Request $request the request
     * 
     * @View(serializerGroups={"user"}, populateDefaultVars=false)
     * @Rest\Put("/user/{username}", name="user-update")
     */
    public function update(string $username, Request $request)
    {
        ///Find user
        $user = $this->userRepository->findOneByUsername($username);

        //Check if the user exists
        if(!$user)
        {
            return new JsonResponse(array('message' => 'User not found'), Response::HTTP_NOT_FOUND);
        }

        //Check if the logged user is the updated user
        if(empty($this->getUser()) || $this->getUser()->getUsername() != $username)
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to modify this user'), Response::HTTP_NOT_FOUND);
        }

        //Parse request body
        $body = json_decode($request->getContent(), true);
        if(empty($body) || !is_array($body)) {
            return new JsonResponse(array('message' => 'Incorrect body format'), Response::HTTP_BAD_REQUEST);
        }

        //If password request is not empty, we change it
        if(!empty($body['password']))
        {
            $user->setPassword($this->passwordEncoder->encodePassword($user, $body['password']));
        }

        //If username has changed, we check if the username aldready exists
        if(!empty($body['username']) && $user->getUsername() !== $body['username'])
        {
            if($this->userRepository->findOneByUsername($body['username']))
            {
                return new JsonResponse(array([
                    'property' => 'username',
                    'message' => 'This username aldready exists',
                ]), 400);
            }
            else
            {
                $user->setUsername($body['username']);
            }
        }

        $errors = [];
        if($this->entityUtility->validate($user, $errors))
        {
            //Save the changes in the database
            $this->entityManager->flush();
            return $user;
        }
        else
        {
            return new JsonResponse($errors, 400);
        }
    }

    /**
     * Delete an user
     * 
     * @param string $username of the user to delete
     * 
     * @View(serializerGroups={"user"}, populateDefaultVars=false)
     * @Rest\Delete("/user/{username}", name="user-delete")
     */
    public function delete(string $username)
    {
        ///Find user
        $user = $this->userRepository->findOneByUsername($username);

        //Check if the user exists
        if(!$user)
        {
            return new JsonResponse(array('message' => 'User not found'), Response::HTTP_NOT_FOUND);
        }

        //check if the logged user is the updated user
        if(empty($this->getUser()) || $this->getUser()->getUsername() !== $username)
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to delete this user'), Response::HTTP_NOT_FOUND);
        }

        //Delete the user
        $this->entityManager->remove($user);
        $this->entityManager->flush();

        return true;
    }

    /**
     * Get current logged user
     * 
     * @View(serializerGroups={"user"}, populateDefaultVars=false)
     * @Rest\Get("/current/user", name="current-user")
     */
    public function currentUser()
    {
        $user = $this->getUser();
        return $user;
    }

    /**
     * Generate a new access token for a given user
     * 
     * @param Request $request
     * @param UsernamePasswordAuthenticator $authenticator
     * @Rest\Post("/login", name="login")
     */
    public function login(Request $request, UsernamePasswordAuthenticator $authenticator)
    {
        //Parse request body
        $body = json_decode($request->getContent(), true);
        if(empty($body) || !is_array($body)) {
            return new JsonResponse(array('message' => 'Incorrect body format'), Response::HTTP_BAD_REQUEST);
        }

        //check if the user exists
        $user = $this->userRepository->findOneByUsername($body['username']);
        if($user)
        {
            //Check if password is valid
            if($this->passwordEncoder->isPasswordValid($user, $body['password']))
            {
                $token = $this->tokenRepository->createByUser($user);
                $this->entityManager->persist($token);
                $this->entityManager->flush();
                return new Response($token->getToken());
            }
            else
            {
                return new JsonResponse(array([
                    'property' => 'password',
                    'message' => 'Invalid password',
                ]), Response::HTTP_FORBIDDEN);
            }
        }
        else
        {
            return new JsonResponse(array([
                'property' => 'username',
                'message' => 'User not found',
            ]), Response::HTTP_NOT_FOUND);
        }
    }
}
