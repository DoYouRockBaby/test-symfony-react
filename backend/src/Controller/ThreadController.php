<?php

namespace App\Controller;

use App\Entity\Area;
use App\Entity\Section;
use App\Entity\Thread;
use App\Entity\User;
use App\Entity\LoginRequest;
use App\Repository\ThreadRepository;
use App\Repository\SectionRepository;
use App\Security\UsernamePasswordAuthenticator;
use App\Utility\EntityUtility;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class ThreadController extends AbstractController
{
    /** @var EntityUtility */
    private $entityUtility;

    /** @var ThreadRepository */
    private $threadRepository;

    /** @var SectionRepository */
    private $sectionRepository;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var TranslatorInterface */
    private $translator;

    /**
     * Constructor, use dependency injection to grab depedencies
     *
     * @param EntityUtility $entityUtility
     * @param ThreadRepository $threadRepository
     * @param SectionRepository $sectionRepository
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityUtility $entityUtility, ThreadRepository $threadRepository, SectionRepository $sectionRepository, EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->entityUtility = $entityUtility;
        $this->threadRepository = $threadRepository;
        $this->sectionRepository = $sectionRepository;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * Get thread informations
     * 
     * @param Thread $thread to display
     * 
     * @View(serializerGroups={"thread"}, populateDefaultVars=false)
     * @Rest\Get("/area/{area_id}/section/{section_id}/thread/{thread_id}", name="thread-get")
     * @ParamConverter("area", options={"id" = "area_id"})
     * @ParamConverter("section", options={"id" = "section_id"})
     * @ParamConverter("thread", options={"id" = "thread_id"})
     */
    public function find(Area $area, Section $section, Thread $thread)
    {
        //Check if the section is located into the area
        if($section->getArea()->getId() != $area->getId())
        {
            return new JsonResponse(array('message' => 'This section is not located into this area'), Response::HTTP_NOT_FOUND);
        }

        //Check if the thread is located into the section
        if($thread->getSection()->getId() != $section->getId())
        {
            return new JsonResponse(array('message' => 'This thread is not located into this section'), Response::HTTP_NOT_FOUND);
        }

        //Check if the logged user is an user for the thread
        if(empty($this->getUser()) || !$area->getUsers()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to see this thread'), Response::HTTP_NOT_FOUND);
        }

        return $thread;
    }

    /**
     * Create a new thread
     * 
     * @param Area $area of the thread to create
     * @param Section $section of the thread to create
     * @param Thread $thread to create
     * 
     * @View(serializerGroups={"thread"}, populateDefaultVars=false)
     * @Rest\Post("/area/{area_id}/section/{section_id}/thread", name="thread-create")
     * @ParamConverter("area", options={"id" = "area_id"})
     * @ParamConverter("section", options={"id" = "section_id"})
     * @ParamConverter("thread", converter="fos_rest.request_body")
     */
    public function create(Area $area, Section $section, Thread $thread)
    {
        //Check if the section is located into the area
        if($section->getArea()->getId() != $area->getId())
        {
            return new JsonResponse(array('message' => 'This section is not located into this area'), Response::HTTP_NOT_FOUND);
        }

        $errors = [];

        $thread->setSection($section);

        if($this->entityUtility->validate($thread, $errors))
        {
            //Check if the logged user is an admin for the thread
            if(empty($this->getUser()) || !$area->getAdmins()->exists(function($key, User $value) {
                return $value->getId() === $this->getUser()->getId();
            }))
            {
                return new JsonResponse(array('message' => 'You\'r not allowed to create this thread'), Response::HTTP_NOT_FOUND);
            }

            //check if the thread title aldready exists
            if($this->threadRepository->findOneByTitle($thread->getTitle()))
            {
                return new JsonResponse(array([
                    'property' => 'title',
                    'message' => 'This title aldready exists',
                ]), 400);
            }

            //Insert the thread in the database
            $this->entityManager->persist($thread);
            $this->entityManager->flush();

            return $thread;
        }
        else
        {
            return new JsonResponse($errors, 400);
        }
    }

    /**
     * Update thread informations
     * 
     * @param int $id of the thread to update
     * @param Request $request the request
     * 
     * @View(serializerGroups={"thread"}, populateDefaultVars=false)
     * @Rest\Put("/area/{areaId}/section/{sectionId}/thread/{id}", name="thread-update")
     */
    public function update(int $areaId, int $sectionId, int $id, Request $request)
    {
        ///Find section
        $section = $this->sectionRepository->find($sectionId);

        //Check if the section exists
        if(!$section)
        {
            return new JsonResponse(array('message' => 'Section not found'), Response::HTTP_NOT_FOUND);
        }

        //Check if the section is located into the area
        if($section->getArea()->getId() != $areaId)
        {
            return new JsonResponse(array('message' => 'This section is not located into this area'), Response::HTTP_NOT_FOUND);
        }

        ///Find thread
        $thread = $this->threadRepository->find($id);

        //Check if the thread exists
        if(!$thread)
        {
            return new JsonResponse(array('message' => 'Thread not found'), Response::HTTP_NOT_FOUND);
        }

        //Check if the thread is located into the section
        if($thread->getSection()->getId() != $sectionId)
        {
            return new JsonResponse(array('message' => 'This thread is not located into this section'), Response::HTTP_NOT_FOUND);
        }

        //Check if the logged user is an admin for the thread
        if(empty($this->getUser()) || !$section->getArea()->getAdmins()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to modify this thread'), Response::HTTP_NOT_FOUND);
        }

        //Parse request body
        $body = json_decode($request->getContent(), true);
        if(empty($body) || !is_array($body)) {
            return new JsonResponse(array('message' => 'Incorrect body format'), Response::HTTP_BAD_REQUEST);
        }

        //If title request is not empty, we change it
        if(!empty($body['title']))
        {
            $thread->setTitle($body['title']);
        }

        $errors = [];
        if($this->entityUtility->validate($thread, $errors))
        {
            //Save the changes in the database
            $this->entityManager->flush();
            return $thread;
        }
        else
        {
            return new JsonResponse($errors, 400);
        }
    }

    /**
     * Delete an thread
     * 
     * @param int $id of the thread to delete
     * 
     * @View(serializerGroups={"thread"}, populateDefaultVars=false)
     * @Rest\Delete("/area/{areaId}/section/{sectionId}/thread/{id}", name="thread-delete")
     */
    public function delete(int $areaId, int $sectionId, int $id)
    {
        ///Find section
        $section = $this->sectionRepository->find($sectionId);

        //Check if the section exists
        if(!$section)
        {
            return new JsonResponse(array('message' => 'Section not found'), Response::HTTP_NOT_FOUND);
        }

        //Check if the section is located into the area
        if($section->getArea()->getId() != $areaId)
        {
            return new JsonResponse(array('message' => 'This section is not located into this area'), Response::HTTP_NOT_FOUND);
        }

        ///Find thread
        $thread = $this->threadRepository->find($id);

        //Check if the thread exists
        if(!$thread)
        {
            return new JsonResponse(array('message' => 'Thread not found'), Response::HTTP_NOT_FOUND);
        }

        //Check if the thread is located into the section
        if($thread->getSection()->getId() != $sectionId)
        {
            return new JsonResponse(array('message' => 'This thread is not located into this section'), Response::HTTP_NOT_FOUND);
        }

        //Check if the logged user is an admin for the thread
        if(empty($this->getUser()) || !$section->getArea()->getAdmins()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to modify this thread'), Response::HTTP_NOT_FOUND);
        }

        //Delete the thread
        $this->entityManager->remove($thread);
        $this->entityManager->flush();

        return true;
    }
}
