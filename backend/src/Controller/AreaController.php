<?php

namespace App\Controller;

use App\Entity\Area;
use App\Entity\User;
use App\Entity\LoginRequest;
use App\Repository\AreaRepository;
use App\Repository\UserRepository;
use App\Security\UsernamePasswordAuthenticator;
use App\Utility\EntityUtility;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class AreaController extends AbstractController
{
    /** @var EntityUtility */
    private $entityUtility;

    /** @var AreaRepository */
    private $areaRepository;

    /** @var UserRepository */
    private $userRepository;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var TranslatorInterface */
    private $translator;

    /**
     * Constructor, use dependency injection to grab depedencies
     *
     * @param EntityUtility $entityUtility
     * @param AreaRepository $areaRepository
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityUtility $entityUtility, AreaRepository $areaRepository, UserRepository $userRepository, EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->entityUtility = $entityUtility;
        $this->areaRepository = $areaRepository;
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * Search among areas
     * 
     * @param string $searchString
     * 
     * @View(serializerGroups={"summary"}, populateDefaultVars=false)
     * @Rest\Get("/area/search/{searchString}", name="area-search")
     */
    public function search(string $searchString)
    {
        return $this->areaRepository->search($searchString);
    }

    /**
     * Get area informations
     * 
     * @param Area $area to display
     * 
     * @View(serializerGroups={"area"}, populateDefaultVars=false)
     * @Rest\Get("/area/{id}", name="area-get")
     */
    public function find(Area $area)
    {
        return $area;
    }

    /**
     * Create a new area
     * 
     * @param Area $area to create
     * 
     * @View(serializerGroups={"area"}, populateDefaultVars=false)
     * @Rest\Post("/area", name="area-create")
     * @ParamConverter("area", converter="fos_rest.request_body")
     */
    public function create(Area $area)
    {
        $errors = [];
        
        //Add the current logged user as an admin for the area
        $area->setAdmins(new ArrayCollection([ $this->getUser() ]));
        $area->setUsers(new ArrayCollection([ $this->getUser() ]));

        if($this->entityUtility->validate($area, $errors))
        {
            //check if the area title aldready exists
            if($this->areaRepository->findOneByTitle($area->getTitle()))
            {
                return new JsonResponse(array([
                    'property' => 'title',
                    'message' => 'This title aldready exists',
                ]), 400);
            }

            //Insert the area in the database
            $this->entityManager->persist($area);
            $this->entityManager->flush();

            return $area;
        }
        else
        {
            return new JsonResponse($errors, 400);
        }
    }

    /**
     * Update area informations
     * 
     * @param int $id of the area to update
     * @param Request $request the request
     * 
     * @View(serializerGroups={"area"}, populateDefaultVars=false)
     * @Rest\Put("/area/{id}", name="area-update")
     */
    public function update(int $id, Request $request)
    {
        ///Find area
        $area = $this->areaRepository->find($id);

        //Check if the area exists
        if(!$area)
        {
            return new JsonResponse(array('message' => 'Area not found'), Response::HTTP_NOT_FOUND);
        }

        //Check if the logged user is an admin for the area
        if(empty($this->getUser()) || !$area->getAdmins()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to modify this area'), Response::HTTP_NOT_FOUND);
        }

        //Parse request body
        $body = json_decode($request->getContent(), true);
        if(empty($body) || !is_array($body)) {
            return new JsonResponse(array('message' => 'Incorrect body format'), Response::HTTP_BAD_REQUEST);
        }

        //If title request is not empty, we change it
        if(!empty($body['title']))
        {
            $area->setTitle($body['title']);
        }

        $errors = [];
        if($this->entityUtility->validate($area, $errors))
        {
            //Save the changes in the database
            $this->entityManager->flush();
            return $area;
        }
        else
        {
            return new JsonResponse($errors, 400);
        }
    }

    /**
     * Delete an area
     * 
     * @param int $id of the area to delete
     * 
     * @View(serializerGroups={"area"}, populateDefaultVars=false)
     * @Rest\Delete("/area/{id}", name="area-delete")
     */
    public function delete(int $id)
    {
        ///Find area
        $area = $this->areaRepository->find($id);

        //Check if area exists
        if(!$area)
        {
            return new JsonResponse(array('message' => 'Area not found'), Response::HTTP_NOT_FOUND);
        }

        //Check if the logged user is an admin for the area
        if(empty($this->getUser()) || !$area->getAdmins()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to delete this area'), Response::HTTP_NOT_FOUND);
        }

        //Delete the area
        $this->entityManager->remove($area);
        $this->entityManager->flush();

        return true;
    }

    /**
     * Add an user to the area
     * 
     * @param Area $area where the user will be added
     * 
     * @View(serializerGroups={"area"}, populateDefaultVars=false)
     * @Rest\Post("/area/{id}/user", name="area-add-user")
     */
    public function addUser(Area $area, Request $request)
    {
        //Check if the logged user is an admin for the area
        if(empty($this->getUser()) || !$area->getAdmins()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to modify this area'), Response::HTTP_NOT_FOUND);
        }

        //Parse request body
        $body = json_decode($request->getContent(), true);
        if(empty($body) || !is_array($body)) {
            return new JsonResponse(array('message' => 'Incorrect body format'), Response::HTTP_BAD_REQUEST);
        }

        //Add the users to the area
        foreach($body as $username) {
            $user = $this->userRepository->findOneByUsername($username);

            //Check if the user exists into the database
            if(empty($user)) {
                return new JsonResponse(array('message' => 'User ' . $user . 'doesn\'t exists'), Response::HTTP_BAD_REQUEST);
            }

            //Check if the area aldready contains the user
            if(!$area->getUsers()->exists(function($key, User $value) use ($user) {
                return $value->getId() === $user->getId();
            }))
            {
                $area->getUsers()->add($user);
            }
        }

        //Save the changes in the database
        $this->entityManager->flush();
        return $area;
    }

    /**
     * Remove an user from the area
     * 
     * @param Area $area where the user will be removed
     * 
     * @View(serializerGroups={"area"}, populateDefaultVars=false)
     * @Rest\Delete("/area/{id}/user", name="area-remove-user")
     */
    public function removeUser(Area $area, Request $request)
    {
        //Check if the logged user is an admin for the area
        if(empty($this->getUser()) || !$area->getAdmins()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to modify this area'), Response::HTTP_NOT_FOUND);
        }

        //Parse request body
        $body = json_decode($request->getContent(), true);
        if(empty($body) || !is_array($body)) {
            return new JsonResponse(array('message' => 'Incorrect body format'), Response::HTTP_BAD_REQUEST);
        }

        //Remove the users from the area
        foreach($body as $username) {
            $user = $this->userRepository->findOneByUsername($username);

            //Check if the user exists into the database
            if(empty($user)) {
                return new JsonResponse(array('message' => 'User ' . $user . 'doesn\'t exists'), Response::HTTP_BAD_REQUEST);
            }

            //Check if the area aldready contains the user
            if($area->getUsers()->exists(function($key, User $value) use ($user) {
                return $value->getId() === $user->getId();
            }))
            {
                //And if the user is not an admin for the area
                if(!$area->getAdmins()->exists(function($key, User $value) use ($user) {
                    return $value->getId() === $user->getId();
                }))
                {
                    $area->getUsers()->remove($user);
                }
            }
        }

        //Save the changes in the database
        $this->entityManager->flush();
        return $area;
    }

    /**
     * Add an user to the area as an admin
     * 
     * @param Area $area where the admin will be added
     * 
     * @View(serializerGroups={"area"}, populateDefaultVars=false)
     * @Rest\Post("/area/{id}/admin", name="area-add-admin")
     */
    public function addAdmin(Area $area, Request $request)
    {
        //Check if the logged user is an admin for the area
        if(empty($this->getUser()) || !$area->getAdmins()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to modify this area'), Response::HTTP_NOT_FOUND);
        }

        //Parse request body
        $body = json_decode($request->getContent(), true);
        if(empty($body) || !is_array($body)) {
            return new JsonResponse(array('message' => 'Incorrect body format'), Response::HTTP_BAD_REQUEST);
        }

        //Add the users to the area
        foreach($body as $username) {
            $user = $this->userRepository->findOneByUsername($username);

            //Check if the user exists into the database
            if(empty($user)) {
                return new JsonResponse(array('message' => 'User ' . $user . 'doesn\'t exists'), Response::HTTP_BAD_REQUEST);
            }

            //Check if the area aldready contains the user
            if(!$area->getUsers()->exists(function($key, User $value) use ($user) {
                return $value->getId() === $user->getId();
            }))
            {
                $area->getUsers()->add($user);
            }

            //Check if the area aldready contains the user as an admin
            if(!$area->getAdmins()->exists(function($key, User $value) use ($user) {
                return $value->getId() === $user->getId();
            }))
            {
                $area->getAdmins()->add($user);
            }
        }

        //Save the changes in the database
        $this->entityManager->flush();
        return $area;
    }

    /**
     * Remove an user from the area
     * 
     * @param Area $area where the admin will be removed
     * 
     * @View(serializerGroups={"area"}, populateDefaultVars=false)
     * @Rest\Delete("/area/{id}/admin", name="area-remove-admin")
     */
    public function removeAdmin(Area $area)
    {
        //Check if the logged user is an admin for the area
        if(empty($this->getUser()) || !$area->getAdmins()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to modify this area'), Response::HTTP_NOT_FOUND);
        }

        //Check if the area aldready contains the user
        if($area->getAdmins()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            //Remove the users from the area
            $area->getAdmins()->removeElement($this->getUser());
        }

        //Save the changes in the database
        $this->entityManager->flush();
        return $area;
    }

    /**
     * Allow the logged user to leave an area
     * 
     * @param Area $area where the admin will be removed
     * 
     * @View(serializerGroups={"area"}, populateDefaultVars=false)
     * @Rest\Delete("/area/{id}/leave", name="area-leave")
     */
    public function leave(Area $area)
    {
        //Check if the area aldready contains the user
        if($area->getUsers()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            //Remove the users from the area
            $area->getAdmins()->removeElement($this->getUser());
            $area->getUsers()->removeElement($this->getUser());
        }

        //Save the changes in the database
        $this->entityManager->flush();
        return $area;
    }
}
