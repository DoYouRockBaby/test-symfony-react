<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\Area;
use App\Entity\Section;
use App\Entity\Thread;
use App\Entity\User;
use App\Entity\LoginRequest;
use App\Repository\MessageRepository;
use App\Repository\ThreadRepository;
use App\Repository\SectionRepository;
use App\Security\UsernamePasswordAuthenticator;
use App\Utility\EntityUtility;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class MessageController extends AbstractController
{
    /** @var EntityUtility */
    private $entityUtility;

    /** @var MessageRepository */
    private $messageRepository;

    /** @var ThreadRepository */
    private $threadRepository;

    /** @var SectionRepository */
    private $sectionRepository;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var TranslatorInterface */
    private $translator;

    /**
     * Constructor, use dependency injection to grab depedencies
     *
     * @param EntityUtility $entityUtility
     * @param MessageRepository $messageRepository
     * @param ThreadRepository $threadRepository
     * @param SectionRepository $sectionRepository
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityUtility $entityUtility, MessageRepository $messageRepository, ThreadRepository $threadRepository, SectionRepository $sectionRepository, EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->entityUtility = $entityUtility;
        $this->messageRepository = $messageRepository;
        $this->threadRepository = $threadRepository;
        $this->sectionRepository = $sectionRepository;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * Search among messages
     * 
     * @param string $searchString
     * 
     * @View(serializerGroups={"summary"}, populateDefaultVars=false)
     * @Rest\Get("/area/{area_id}/section/{section_id}/thread/{thread_id}/message/search/{searchString}", name="message-search")
     * @ParamConverter("area", options={"id" = "area_id"})
     * @ParamConverter("section", options={"id" = "section_id"})
     * @ParamConverter("thread", options={"id" = "thread_id"})
     */
    public function search(Area $area, Section $section, Thread $thread, string $searchString)
    {
        //Check if the section is located into the area
        if($section->getArea()->getId() != $area->getId())
        {
            return new JsonResponse(array('message' => 'This section is not located into this area'), Response::HTTP_NOT_FOUND);
        }

        //Check if the thread is located into the section
        if($thread->getSection()->getId() != $section->getId())
        {
            return new JsonResponse(array('message' => 'This thread is not located into this section'), Response::HTTP_NOT_FOUND);
        }

        //Check if the logged user is an user for the thread
        if(empty($this->getUser()) || !$area->getUsers()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to search among this thread'), Response::HTTP_NOT_FOUND);
        }

        return $this->messageRepository->search($searchString, $thread->getSection()->getId());
    }

    /**
     * Get thread informations
     * 
     * @param Thread $thread to display
     * 
     * @View(serializerGroups={"message"}, populateDefaultVars=false)
     * @Rest\Get("/area/{area_id}/section/{section_id}/thread/{thread_id}/messages/{offset}/{limit}", name="message-find-thread-offset-size")
     * @ParamConverter("area", options={"id" = "area_id"})
     * @ParamConverter("section", options={"id" = "section_id"})
     * @ParamConverter("thread", options={"id" = "thread_id"})
     */
    public function findByThreadWithOffsetAndLimit(Area $area, Section $section, Thread $thread, int $offset, int $limit)
    {
        //Check if the section is located into the area
        if($section->getArea()->getId() != $area->getId())
        {
            return new JsonResponse(array('message' => 'This section is not located into this area'), Response::HTTP_NOT_FOUND);
        }

        //Check if the thread is located into the section
        if($thread->getSection()->getId() != $section->getId())
        {
            return new JsonResponse(array('message' => 'This thread is not located into this section'), Response::HTTP_NOT_FOUND);
        }

        //Check if the logged user is an user for the thread
        if(empty($this->getUser()) || !$area->getUsers()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to see this thread'), Response::HTTP_NOT_FOUND);
        }

        return $this->messageRepository->findByThreadWithOffsetAndLimit($thread->getSection()->getId(), $offset, $limit);
    }

    /**
     * Get thread informations
     * 
     * @param Thread $thread to display
     * 
     * @View(serializerGroups={"message"}, populateDefaultVars=false)
     * @Rest\Get("/area/{area_id}/section/{section_id}/thread/{thread_id}/message/{message_id}", name="message-get")
     * @ParamConverter("area", options={"id" = "area_id"})
     * @ParamConverter("section", options={"id" = "section_id"})
     * @ParamConverter("thread", options={"id" = "thread_id"})
     * @ParamConverter("message", options={"id" = "message_id"})
     */
    public function find(Area $area, Section $section, Thread $thread, Message $message)
    {
        //Check if the section is located into the area
        if($section->getArea()->getId() != $area->getId())
        {
            return new JsonResponse(array('message' => 'This section is not located into this area'), Response::HTTP_NOT_FOUND);
        }

        //Check if the thread is located into the section
        if($thread->getSection()->getId() != $section->getId())
        {
            return new JsonResponse(array('message' => 'This thread is not located into this section'), Response::HTTP_NOT_FOUND);
        }

        //Check if the message is located into the thread
        if($message->getThread()->getId() != $thread->getId())
        {
            return new JsonResponse(array('message' => 'This message is not located into this thread'), Response::HTTP_NOT_FOUND);
        }

        //Check if the logged user is an user for the thread
        if(empty($this->getUser()) || !$area->getUsers()->exists(function($key, User $value) {
            return $value->getId() === $this->getUser()->getId();
        }))
        {
            return new JsonResponse(array('message' => 'You\'r not allowed to see this message'), Response::HTTP_NOT_FOUND);
        }

        return $thread;
    }

    /**
     * Create a new thread
     * 
     * @param Area $area of the thread to create
     * @param Section $section of the thread to create
     * @param Thread $thread of the thread to create
     * @param Message $message to create
     * 
     * @View(serializerGroups={"message"}, populateDefaultVars=false)
     * @Rest\Post("/area/{area_id}/section/{section_id}/thread/{thread_id}/message", name="message-create")
     * @ParamConverter("area", options={"id" = "area_id"})
     * @ParamConverter("section", options={"id" = "section_id"})
     * @ParamConverter("thread", options={"id" = "thread_id"})
     * @ParamConverter("message", converter="fos_rest.request_body")
     */
    public function create(Area $area, Section $section, Thread $thread, Message $message)
    {
        $user = $this->getUser();

        //Check if the section is located into the area
        if($section->getArea()->getId() != $area->getId())
        {
            return new JsonResponse(array('message' => 'This section is not located into this area'), Response::HTTP_NOT_FOUND);
        }

        //Check if the thread is located into the section
        if($thread->getSection()->getId() != $section->getId())
        {
            return new JsonResponse(array('message' => 'This thread is not located into this section'), Response::HTTP_NOT_FOUND);
        }

        $errors = [];

        $message->setThread($thread);
        $message->setDatetime(new \DateTime('now'));
        $message->setUser($user);

        if($this->entityUtility->validate($message, $errors))
        {
            //Check if the logged user is an admin for the message
            if(empty($user) || !$area->getUsers()->exists(function($key, User $value) {
                return $value->getId() === $user->getId();
            }))
            {
                return new JsonResponse(array('message' => 'You\'r not allowed to create this message'), Response::HTTP_NOT_FOUND);
            }

            //Insert the message in the database
            $this->entityManager->persist($message);
            $this->entityManager->flush();

            return $message;
        }
        else
        {
            return new JsonResponse($errors, 400);
        }
    }
}
