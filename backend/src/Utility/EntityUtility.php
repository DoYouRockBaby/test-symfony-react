<?php

namespace App\Utility;

use Symfony\Component\Validator\Validator\ValidatorInterface;

class EntityUtility
{
    /** @var ValidatorInterface */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Validate an entity
     * 
     * @param mixed $entity the entity to validate
     * @param array the error messages, formated as [['message' : 'my error message', 'property' : 'my_property_name'], ...]
     * @return bool
     */
    public function validate($entity, array &$errors): bool
    {
        $errors = [];
        $validations = $this->validator->validate($entity);
        foreach($validations as $validation)
        {
            $errors[] = [
                'property' => $validation->getPropertyPath(),
                'message' => $validation->getMessage(),
            ];
        }

        return count($validations) == 0;
    }
}