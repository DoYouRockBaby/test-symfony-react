<?php

namespace App\Repository;

use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Message::class);
    }

    public function search(string $query, int $thread): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.message LIKE :query AND a.thread = :thread')
            ->setParameter('query', '%' . $query . '%')
            ->setParameter('thread', $thread)
            ->addOrderBy('a.datetime', 'DESC')
            ->addOrderBy('a.id', 'DESC')
            ->setMaxResults(30)
            ->getQuery()
            ->getResult();
    }

    public function findByThreadWithOffsetAndLimit(int $thread, int $offset, int $limit): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.thread = :thread')
            ->setParameter('thread', $thread)
            ->addOrderBy('a.datetime', 'DESC')
            ->addOrderBy('a.id', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
