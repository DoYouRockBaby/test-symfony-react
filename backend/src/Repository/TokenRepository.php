<?php

namespace App\Repository;

use App\Entity\Token;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Token|null find($id, $lockMode = null, $lockVersion = null)
 * @method Token|null findOneBy(array $criteria, array $orderBy = null)
 * @method Token[]    findAll()
 * @method Token[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TokenRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Token::class);
    }

    public function createByUser(User $user): Token
    {
        $token = '';

        do {
            $token = uniqid("", true);
        } while (!empty($this->findOneBy(array('token' => $token))));

        $tokenEntity = new Token();
        $tokenEntity->setToken($token);
        $tokenEntity->setUser($user);
        $tokenEntity->setLastTimeUsed(new \DateTime('now'));

        return $tokenEntity;
    }

    public function findActive(string $token): ?Token
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.token = :token AND t.lastTimeUsed >= :lastTimeUsed')
            ->setParameter('token', $token)
            ->setParameter('lastTimeUsed', (new \DateTime())->modify('-3600 seconds'))
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
