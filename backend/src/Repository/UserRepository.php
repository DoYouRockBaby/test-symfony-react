<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    private $tokenRepository;
    
    public function __construct(RegistryInterface $registry, TokenRepository $tokenRepository)
    {
        parent::__construct($registry, User::class);

        $this->tokenRepository = $tokenRepository;
    }
    
    /**
     * Find a user using its username
     *
     * @param string $username
     * @return User|null
     */
    public function findOneByUsername(string $username): ?User
    {
        return $this->findOneBy(array('username' => $username));
    }

    /**
     * Search a user using a string
     *
     * @param string $query
     * @return array
     */
    public function search(string $query): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.username LIKE :query')
            ->setParameter('query', '%' . $query . '%')
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(15)
            ->getQuery()
            ->getResult();
    }
}
