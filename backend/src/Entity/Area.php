<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AreaRepository")
 */
class Area
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"user", "area", "section", "summary"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     * @Groups({"user", "area", "section", "summary"})
     */
    private $title;

    /**
     * @ORM\ManyToMany(targetEntity="User")
     * @ORM\JoinTable(name="area_admin")
     * @Groups({"area", "section"})
     */
    private $admins;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="areas")
     * @ORM\JoinTable(name="area_user")
     * @Groups({"area", "section"})
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="Section", mappedBy="area")
     * @Groups({"area"})
     */
    private $sections;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUsers(): ?Collection
    {
        return $this->users;
    }

    public function setUsers(Collection $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getAdmins(): ?Collection
    {
        return $this->admins;
    }

    public function setAdmins(Collection $admins): self
    {
        $this->admins = $admins;

        return $this;
    }

    public function getSections(): ?Collection
    {
        return $this->sections;
    }

    public function setSections(Collection $sections): self
    {
        $this->sections = $sections;

        return $this;
    }
}
