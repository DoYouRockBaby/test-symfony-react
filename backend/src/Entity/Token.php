<?php

namespace App\Entity;

use App\Entity\User;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TokenRepository")
 */
class Token
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Length(max=255)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;
    
    /**
     * @ORM\Column(type="datetime")
     */
    private $lastTimeUsed;

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getLastTimeUsed(): ?\DateTime
    {
        return $this->lastTimeUsed;
    }

    public function setLastTimeUsed(\DateTime $lastTimeUsed): self
    {
        $this->lastTimeUsed = $lastTimeUsed;

        return $this;
    }
}
