<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SectionRepository")
 */
class Section
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"area", "section", "thread", "summary"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     * @Groups({"area", "section", "thread", "summary"})
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="Area", inversedBy="sections")
     * @Groups({"section"})
     */
    private $area;

    /**
     * @ORM\OneToMany(targetEntity="Thread", mappedBy="section")
     * @Groups({"area", "section"})
     */
    private $threads;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getArea(): ?Area
    {
        return $this->area;
    }

    public function setArea(Area $area): self
    {
        $this->area = $area;

        return $this;
    }

    public function getThreads(): ?Collection
    {
        return $this->threads;
    }

    public function setThreads(Collection $threads): self
    {
        $this->threads = $threads;

        return $this;
    }
}
