<?php

namespace App\Security;

use App\Entity\User;
use App\Repository\TokenRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Translation\TranslatorInterface;

class UsernamePasswordAuthenticator extends AbstractGuardAuthenticator
{
    private $entityManager;
    private $translator;
    private $tokenRepository;
    private $userRepository;

    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator, TokenRepository $tokenRepository, UserRepository $userRepository)
    {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
        $this->tokenRepository = $tokenRepository;
        $this->userRepository = $userRepository;
    }

    public function supports(Request $request)
    {
        return $request->request->has('username') && $request->request->has('password');
    }

    public function getCredentials(Request $request)
    {
        return array(
            'username' => $request->request->get('username'),
            'password' => $request->request->get('password'),
        );
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $username = $credentials['username'];
        $password = $credentials['password'];

        if ($username === null || $password === null)
        {
            return;
        }

        return $this->entityManager->getRepository(User::class)->findOneByUsername($username);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = array(
            'message' => $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        );

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            'message' => $this->translator->trans('Authentication Required')
        );

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
