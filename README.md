How to launch the application :
- docker-compose up

What are the urls :
- Web UI : localhost:8080
- Web API : localhost:8081
- PHPMyAdmin : localhost:8082 (use "user" as user and "password" as password)