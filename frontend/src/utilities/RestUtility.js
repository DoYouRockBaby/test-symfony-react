import axios from 'axios';
import api from '../configs/api';
import type { ErrorMessage } from '../types/ErrorMessage';
import cookieUtility from "../utilities/CookieUtility";

let currentToken: ?string = null;

function generateHeaders(jsonBody: bool): any {
    let headers: any = {};

    if(jsonBody) {
        headers['Content-Type'] = 'application/json';
    }
    
    if(currentToken !== null && currentToken !== '') {
        headers['Authorization'] = currentToken;
    }

    return headers;
}

export class RestUtility
{
    constructor() {
        currentToken = cookieUtility.getCookie('token');
    }

    hasToken(): bool {
        return currentToken !== null && currentToken !== ''
    }

    getToken(): ?string {
        return currentToken
    }

    useToken(token: string) {
        currentToken = token
        cookieUtility.setCookie('token', token, 30);
    }

    get(path: string, onSuccess: (data: any) => void, onError: (errors: Array<ErrorMessage>) => void) {
        axios.get(api.url + path, {
            headers: generateHeaders(false),
        })
        .then(function (response) {
            onSuccess(response.data)
        })
        .catch(function (err) {
            onError(err.response.data)
        });
    }

    post(path: string, parameters: any, onSuccess: (data: any) => void, onError: (errors: Array<ErrorMessage>) => void) {
        axios.post(api.url + path, parameters, {
            headers: generateHeaders(true),
        })
        .then(function (response) {
            onSuccess(response.data)
        })
        .catch(function (err) {
            onError(err.response.data)
        });
    }

    put(path: string, parameters: any, onSuccess: (data: any) => void, onError: (errors: Array<ErrorMessage>) => void) {
        axios.put(api.url + path, parameters, {
            headers: generateHeaders(true),
        })
        .then(function (response) {
            onSuccess(response.data)
        })
        .catch(function (err) {
            onError(err.response.data)
        });
    }

    delete(path: string, onSuccess: (data: any) => void, onError: (errors: Array<ErrorMessage>) => void) {
        axios.delete(api.url + path, {
            headers: generateHeaders(false),
        })
        .then(function (response) {
            onSuccess(response.data)
        })
        .catch(function (err) {
            onError(err.response.data)
        });
    }
}

const restUtility: RestUtility = new RestUtility()

export default restUtility