import {isArray } from "util";
import type { ErrorMessage } from '../types/ErrorMessage';

export function messageFromProperty(property: string, errors: Array<ErrorMessage>): string {
    let messages = errors.filter(e => e.property === property);
    
    if(!isArray(messages) || messages.length === 0) {
        return '';
    } else {
        return (typeof messages[0].message === 'string' && messages[0].message !== '') ? messages[0].message : '';
    }
}