import type { ErrorMessage } from '../types/ErrorMessage';
import type { Dispatch } from 'redux';
import restUtility from "../utilities/RestUtility";
import type { Area, Section, Thread } from '../types/Entity'

export const createThread = (thread: Thread, section: Section, area: Area) => {
    return function(dispatch: Dispatch<any>) {
        return restUtility.post('area/' + area.id + '/section/' + section.id + '/thread', thread, function (thread: Thread) {
            dispatch({
                type: 'THREAD_CREATED',
                thread: thread,
            })
        }, function (errors: Array<ErrorMessage>) {
            console.log(errors)
        })
    }
}

export const messageChange = (val: string) => ({
    type: 'MESSAGE_CHANGE',
    message: val,
})