import type { ErrorMessage } from '../types/ErrorMessage';
import type { Dispatch } from 'redux';
import restUtility from "../utilities/RestUtility";
import type { Area, Section, URLSearchParams } from '../types/Entity'

export const createSection = (section: Section) => {
    let areaId = section.area.id;
    section.area = undefined;

    return function(dispatch: Dispatch<any>) {
        return restUtility.post('area/' + areaId + '/section', section, function (section: Section) {
            dispatch({
                type: 'SECTION_CREATED',
                section: section,
            })
        }, function (errors: Array<ErrorMessage>) {
        })
    }
}

export const titleChange = (val: string) => ({
    type: 'SECTION_TITLE_CHANGE',
    title: val,
})

export const displaySectionCreation = () => ({
    type: 'SECTION_CREATION_DISPLAY',
})

export const hideSectionCreation = () => ({
    type: 'SECTION_CREATION_HIDE',
})