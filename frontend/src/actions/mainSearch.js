import type { ErrorMessage } from '../types/ErrorMessage';
import type { Dispatch } from 'redux';
import restUtility from "../utilities/RestUtility";
import type { Area } from '../types/Entity'

export const searchRequest = (request: string) => {
    return function(dispatch: Dispatch<any>) {
        dispatch({
            type: 'MAIN_SEARCH_REQUEST_CHANGE',
            request: request,
        })

        dispatch({ type: 'MAIN_SEARCH_REQUEST' })

        return restUtility.get('area/search/' + request, function (areas: Array<Area>) {
            dispatch({
                type: 'MAIN_SEARCH_SUCCESS',
                areas: areas,
            })
        }, function (errors: Array<ErrorMessage>) {
            dispatch({
                type: 'MAIN_SEARCH_ERROR',
                errors: errors,
            })
        });
    }
}