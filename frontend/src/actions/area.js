import type { ErrorMessage } from '../types/ErrorMessage';
import type { Dispatch } from 'redux';
import restUtility from "../utilities/RestUtility";
import type { Area, User } from '../types/Entity'

export const selectArea = (area: Area) => {
    return function(dispatch: Dispatch<any>) {
        return restUtility.get('area/' + area.id, function (area: Area) {
            dispatch({
                type: 'AREA_SELECTED',
                area: area,
            })
        }, function (errors: Array<ErrorMessage>) {
        })
    }
}

export const createArea = (area: Area) => {
    return function(dispatch: Dispatch<any>) {
        return restUtility.post('area', area, function (area: Area) {
            return restUtility.get('current/user', function (user: User) {
                dispatch({
                    type: 'USER_UPDATED',
                    user: user,
                })
                dispatch({
                    type: 'AREA_SELECTION_DISPLAY',
                })
            }, function (errors: Array<ErrorMessage>) {
            })
        }, function (errors: Array<ErrorMessage>) {
        })
    }
}

export const titleChange = (val: string) => ({
    type: 'AREA_TITLE_CHANGE',
    title: val,
})

export const areaDisplayCreation = () => ({
    type: 'AREA_CREATION_DISPLAY',
})

export const areaDisplaySelection = () => ({
    type: 'AREA_SELECTION_DISPLAY',
})