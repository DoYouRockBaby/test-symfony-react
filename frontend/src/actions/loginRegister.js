import type { User } from '../types/Entity';
import type { ErrorMessage } from '../types/ErrorMessage';
import type { Dispatch } from 'redux';
import restUtility from "../utilities/RestUtility";

export const registerRequest = (username: string, password: string) => {
    return function(dispatch: Dispatch<any>) {
        dispatch({ type: 'REGISTER_REQUEST' })

        return restUtility.post('user', {
            username: username,
            password: password
        }, function (user: User) {
            dispatch({
                type: 'REGISTER_SUCCESS',
                user: user,
            })
        }, function (errors: Array<ErrorMessage>) {
            dispatch({
                type: 'REGISTER_ERROR',
                errors: errors,
            })
        });
    }
}

export const loginRequest = (username: string, password: string) => {
    return function(dispatch: Dispatch<any>) {
        dispatch({ type: 'LOGIN_REQUEST' })

        return restUtility.post('login', {
            username: username,
            password: password
        }, function (token) {
            restUtility.useToken(token)

            return restUtility.get('current/user', function (user: User) {
                dispatch({
                    type: 'LOGIN_SUCCESS',
                    user: user,
                })
              }, function (errors: Array<ErrorMessage>) {
                dispatch({
                    type: 'LOGIN_ERROR',
                    errors: errors,
                })
            })
        }, function (errors: Array<ErrorMessage>) {
            dispatch({
                type: 'LOGIN_ERROR',
                errors: errors,
            })
        })
    }
}

export const logUsingCookie = () => {
    return function(dispatch: Dispatch<any>) {
        if(restUtility.hasToken()) {
            return restUtility.get('current/user', function (user: User) {
                dispatch({
                    type: 'LOGIN_SUCCESS',
                    user: user,
                })
            }, function (errors: Array<ErrorMessage>) {
                restUtility.useToken('')
            })
        }
    }
}

export const usernameChange = (val: string) => ({
    type: 'USERNAME_CHANGE',
    username: val,
})

export const passwordChange = (val: string) => ({
    type: 'PASSWORD_CHANGE',
    password: val,
})

export const passwordConfirmChange = (val: string) => ({
    type: 'CONFIRM_PASSWORD_CHANGE',
    confirmPassword: val,
})

export const displayRegister = () => ({
    type: 'DISPLAY_REGISTER',
})

export const displayLogin = () => ({
    type: 'DISPLAY_LOGIN',
})