import React, { Component } from 'react';
import MainSearch from '../../components/MainSearch/index'
import SideMenu from '../../components/SideMenu/index'
import MessageList from '../../components/MessageList/index'
import MessageTextbox from '../../components/MessageTextbox/index'
import AreaSelector from '../../components/AreaSelector/index'
import './index.css';

type Props = {};

class Chat extends Component<Props> {
    render() {
        return (
            <div className="main-container" >
                <header>
                    <nav className="navbar navbar-expand-md navbar-dark static-top bg-dark">
                        <a className="navbar-brand" >Chat</a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse d-flex flex-row" id="navbarCollapse">
                            <MainSearch/>
                            <AreaSelector/>
                        </div>
                    </nav>
                </header>

                <div className="wrapper">
                    <SideMenu/>
                    <div className="content" >
                        <MessageList/>
                        <MessageTextbox/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Chat;
