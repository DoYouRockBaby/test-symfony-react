import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { LoginRegisterDisplayStateEnum } from '../../types/Enum'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import type { Dispatch } from 'redux'
import type { ReducerState } from '../../reducers'
import { displayRegister, loginRequest, usernameChange, passwordChange, logUsingCookie } from '../../actions/loginRegister'

type Props = {
    visible: bool,

    username: string,
    password: string,

    usernameError: string,
    passwordError: string,
    
    onUsernameChange: (string) => void,
    onPasswordChange: (string) => void,
    onSubmit: (string, string) => void,
    onRegister: () => void,
    logUsingCookie: () => void,
}

class Login extends Component<Props> {
    onSubmit = (ev: Event) => {
        ev.preventDefault()

        //Check if username is empty
        if(this.props.username === '') {
            return
        }

        //Check if username is empty
        if(this.props.password === '') {
            return
        }

        //Submit the form
        this.props.onSubmit(this.props.username, this.props.password)
    }

    onRegister = (ev: Event) => {
        ev.preventDefault()
        this.props.onRegister()
    }

    componentWillMount = () => {
        //Try to log using cookie
        this.props.logUsingCookie()
    }

    render() {
        return (
            <div className={"overlay vertical-align" + (this.props.visible ? "" : " d-none")} >
                <div className="container center_div" >
                    <h1>Login</h1>
                    <form onSubmit={this.onSubmit} >
                        <div className="form-group">
                            <label htmlFor="login-username">Username</label>
                            <input type="text" name="username" id="login-username" className={"form-control" + (this.props.usernameError.length > 0 ? ' is-invalid' : '')} value={this.props.username} onChange={this.props.onUsernameChange} />
                            <div className="invalid-feedback">{this.props.usernameError}</div>
                        </div>

                        <div className="form-group">
                            <label htmlFor="login-password">Password</label>
                            <input type="password" name="password" id="login-password" className={"form-control" + (this.props.passwordError.length > 0 ? ' is-invalid' : '')} value={this.props.password} onChange={this.props.onPasswordChange} />
                            <div className="invalid-feedback" >{this.props.passwordError}</div>
                        </div>
                        
                        <div className="form-row" >
                            <div className="form-group col-md-6">
                                <input type="submit" value="Login" className="btn btn-primary" />
                            </div>

                            <div className="form-group col-md-6">
                                <input type="submit" value="Register" className="btn btn-secondary" onClick={this.onRegister} />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
    
    static defaultProps = {
        visible : true,
        usernameError: '',
        passwordError: '',
    }

    static propTypes = {
        visible : PropTypes.bool,
        onSubmit : PropTypes.func.isRequired,
        onRegister : PropTypes.func.isRequired
    }
}

const mapStateToProps = (state: ReducerState) => {
    return {
        visible: state.user.user === null && state.loginRegister.action === LoginRegisterDisplayStateEnum.LOGIN,
        username: state.loginRegister.username,
        password: state.loginRegister.password,
        usernameError: state.loginRegister.usernameError,
        passwordError: state.loginRegister.passwordError,
    }
}

const mapDisplatchToProps = (dispatch: Dispatch<any>) => bindActionCreators({
    onUsernameChange: (ev) => usernameChange(ev.target.value),
    onPasswordChange: (ev) => passwordChange(ev.target.value),
    onSubmit: (username, password) => loginRequest(username, password),
    logUsingCookie: () => logUsingCookie(),
    onRegister: () => displayRegister(),
}, dispatch)

export default connect(mapStateToProps, mapDisplatchToProps)(Login)