import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { LoginRegisterDisplayStateEnum } from '../../types/Enum'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import type { Dispatch } from 'redux'
import type { ReducerState } from '../../reducers'
import { displayLogin, registerRequest, usernameChange, passwordChange, passwordConfirmChange } from '../../actions/loginRegister'

type Props = {
    visible: bool,

    username: string,
    password: string,
    confirmPassword: string,

    usernameError: string,
    passwordError: string,
    confirmPasswordError: string,

    onUsernameChange: (string) => void,
    onPasswordChange: (string) => void,
    onPasswordConfirmChange: (string) => void,
    onSubmit: (string, string) => void,
    onCancel: () => void,
}

class Register extends Component<Props> {
    onSubmit(ev: Event) {
        ev.preventDefault()

        //Check if username is empty
        if(this.props.username === '') {
            return
        }

        //Check if username is empty
        if(this.props.password === '') {
            return
        }

        //Submit the form
        this.props.onSubmit(this.props.username, this.props.password)
    }

    onCancel = (ev: Event) => {
        ev.preventDefault()
        this.props.onCancel()
    }
  
    render() {
        return (
            <div className={"overlay vertical-align" + (this.props.visible ? "" : " d-none")}>
                <div className="container center_div" >
                    <h1>Register a new user</h1>
                    <form onSubmit={(ev) => this.onSubmit(ev)} >
                        <div className="form-group">
                            <label htmlFor="register-username">Username</label>
                            <input type="text" name="username" id="register-username" className={"form-control" + (this.props.usernameError.length > 0 ? ' is-invalid' : '')} value={this.props.username} onChange={this.props.onUsernameChange} />
                            <div className="invalid-feedback">{this.props.usernameError}</div>
                        </div>

                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label htmlFor="register-password">Password</label>
                                <input type="password" name="password" id="register-password" className={"form-control" + (this.props.passwordError.length > 0 ? ' is-invalid' : '')} value={this.props.password} onChange={this.props.onPasswordChange} />
                                <div className="invalid-feedback" >{this.props.passwordError}</div>
                            </div>

                            <div className="form-group col-md-6">
                                <label htmlFor="register-confirm-password">Confirm</label>
                                <input type="password" name="confirm-password" id="register-confirm-password" className={"form-control" + (this.props.confirmPasswordError.length > 0 ? ' is-invalid' : '')} value={this.props.confirmPassword} onChange={this.props.onPasswordConfirmChange} />
                                <div className="invalid-feedback" >{this.props.confirmPasswordError}</div>
                            </div>
                        </div>
                        
                        <div className="form-row" >
                            <div className="form-group col-md-6">
                                <input type="submit" value="Register" className="btn btn-primary" />
                            </div>

                            <div className="form-group col-md-6">
                                <input type="submit" value="Cancel" className="btn btn-secondary" onClick={this.onCancel} />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }

    static defaultProps = {
        visible : true,
        usernameError: '',
        passwordError: '',
    }
    
    static propTypes = {
        visible : PropTypes.bool,
        onSubmit : PropTypes.func.isRequired,
        onCancel : PropTypes.func.isRequired
    }
}

const mapStateToProps = (state: ReducerState) => {
    return {
        visible: state.user.user === null && state.loginRegister.action === LoginRegisterDisplayStateEnum.REGISTER,
        username: state.loginRegister.username,
        password: state.loginRegister.password,
        confirmPassword: state.loginRegister.confirmPassword,
        usernameError: state.loginRegister.usernameError,
        passwordError: state.loginRegister.passwordError,
        confirmPasswordError: state.loginRegister.confirmPasswordError,
    }
}

const mapDisplatchToProps = (dispatch: Dispatch<any>) => bindActionCreators({
    onUsernameChange: (ev) => usernameChange(ev.target.value),
    onPasswordChange: (ev) => passwordChange(ev.target.value),
    onPasswordConfirmChange: (ev) => passwordConfirmChange(ev.target.value),
    onSubmit: (username, password) => registerRequest(username, password),
    onCancel: () => displayLogin(),
}, dispatch)

export default connect(mapStateToProps, mapDisplatchToProps)(Register)