import React, { Component } from 'react';
import { Provider } from 'react-redux'
import Login from './containers/Login';
import Register from './containers/Register';
import Chat from './containers/Chat';
import store from './store';

type Props = {};

class App extends Component<Props> {
    render() {
        return (
            <Provider store={store} >
                <div>
                    <Login/>
                    <Register/>
                    <Chat/>
                </div>
            </Provider>
        );
    }
}

export default App;
