import { createStore, applyMiddleware } from 'redux';
import reducers from '../reducers';
import logger from 'redux-logger'
import thunk from 'redux-thunk';

const middlewares = [
    thunk,
    logger,
];

export default createStore(reducers, applyMiddleware(...middlewares));