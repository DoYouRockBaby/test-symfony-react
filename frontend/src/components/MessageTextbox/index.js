import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import type { Dispatch } from 'redux'
import type { ReducerState } from '../../reducers'
import { messageChange } from '../../actions/thread'
import PropTypes from 'prop-types'
import './index.css'

type Props = {
    message: string,
    onMessageChange: (string) => void,
};

class MessageTextbox extends Component<Props> {
    onChange = (ev: Event) => {
        if (ev.target instanceof HTMLInputElement) {
            if(this.props.onMessageChange !== null) {
                this.props.onMessageChange(ev.target.value)
            }
        }
    }

    render() {
        return (
            <form className="form-horizontal message-form row" >
                <div className="col-sm-10" >
                    <input className="form-control " type="text" placeholder="Your message" value={this.props.message} onChange={this.onChange} />
                </div>
                <input className="btn btn-primary col-sm-2" type="submit" />
            </form>
        );
    }
    
    static defaultProps = {
        message: '',
    }

    static propTypes = {
        message: PropTypes.string,
    }
}

const mapStateToProps = (state: ReducerState) => {
    return {
        message: state.thread.message,
    }
}

const mapDisplatchToProps = (dispatch: Dispatch<any>) => bindActionCreators({
    onMessageChange: (value) => messageChange(value),
}, dispatch)

export default connect(mapStateToProps, mapDisplatchToProps)(MessageTextbox)