import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import type { Dispatch } from 'redux'
import type { ReducerState } from '../../reducers'
import { searchRequest } from '../../actions/mainSearch'
import { selectArea } from '../../actions/area'
import Autocomplete from '../Autocomplete';
import type { Area } from '../../types/Entity';
import PropTypes from 'prop-types'

type SuggestionComponentProps = {
    value: Area,
}

class SuggestionComponent extends Component<SuggestionComponentProps> {
    render() {
        return <span>{ this.props.value.title }</span>
    }
}

type Props = {
    request: string,
    areas: Array<Area>,
    onRequestChange: (string) => void,
    onSuggestionSelected: (any) => void
};

class MainSearch extends Component<Props> {
    render() {
        return (
            <div className="container-fluid" >
                <Autocomplete className="py-1 form-control" onChange={ this.props.onRequestChange } onSuggestionSelected={ this.props.onSuggestionSelected } value={ this.props.request } suggestions={ this.props.areas } suggestionComponent={ SuggestionComponent } />
            </div>
        );
    }
    
    static defaultProps = {
        request: '',
        areas: [],
    }

    static propTypes = {
        request: PropTypes.string,
        areas: PropTypes.array,
    }
}

const mapStateToProps = (state: ReducerState) => {
    return {
        request: state.mainSearch.request,
        error: state.mainSearch.error,
        areas: state.mainSearch.areas,
    }
}

const mapDisplatchToProps = (dispatch: Dispatch<any>) => bindActionCreators({
    onRequestChange: (value) => searchRequest(value),
    onSuggestionSelected: (area) => selectArea(area),
}, dispatch)

export default connect(mapStateToProps, mapDisplatchToProps)(MainSearch)