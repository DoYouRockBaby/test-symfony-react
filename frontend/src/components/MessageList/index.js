import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import type { Dispatch } from 'redux'
import type { ReducerState } from '../../reducers'
import type { Message } from '../../types/Entity';
import PropTypes from 'prop-types'
import './index.css'

type Props = {
    messages: Array<Message>,
};

class MessageList extends Component<Props> {
    render() {
        return (
            <ul className="list-group message-list" >
                { this.props.messages.reverse().map((message: Message) => 
                    <li key={message.id} className="list-group-item message-li" >
                        <div className="card" >
                            <div className="card-body">
                                <h5 className="card-title">{  }</h5>
                                <h6 className="card-subtitle mb-2 text-muted">{ message.datetime.toDateString() }</h6>
                                <p className="card-text">{ message.message }</p>
                            </div>
                        </div>
                    </li>
                )}
            </ul>
        )
    }
    
    static defaultProps = {
        messages: [],
    }

    static propTypes = {
        messages: PropTypes.array,
    }
}

const mapStateToProps = (state: ReducerState) => {
    return {
        messages: state.thread.messages,
    }
}

const mapDisplatchToProps = (dispatch: Dispatch<any>) => bindActionCreators({
}, dispatch)

export default connect(mapStateToProps, mapDisplatchToProps)(MessageList)