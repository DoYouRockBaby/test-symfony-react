import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './index.css'

type DefaultSuggestionComponentProps = {
    value: string,
}

class DefaultSuggestionComponent extends Component<DefaultSuggestionComponentProps> {
    render() {
        return <span>{ this.props.value }</span>
    }

    static defaultProps = {
        value : '',
    }

    static propTypes = {
        value : PropTypes.string,
    }
}

type Props = {
    className: string,
    value: string,
    error: string,
    message: string,
    suggestions: Array<any>,
    onChange: (value: string) => void,
    onSuggestionSelected: (value: void) => void,
    suggestionComponent: any
};

class Autocomplete extends Component<Props> {
    onChange = (ev: Event) => {
        if (ev.target instanceof HTMLInputElement) {
            if(this.props.onChange !== null) {
                this.props.onChange(ev.target.value)
            }
        }
    }

    render() {
        return (
            <React.Fragment>
                <input className={ this.props.className } type="text" placeholder="Search" value={this.props.value} onChange={this.onChange} />
                <div className="autocomplete-suggestions" >
                    <ul className="list-group" >
                        { this.props.message.length > 0 &&
                            <li className="list-group-item" >
                                { this.props.message }
                            </li>
                        }

                        { this.props.error.length > 0 &&
                            <li className="list-group-item list-group-item-danger" >
                                { this.props.error }
                            </li>
                        }

                        { this.props.suggestions.map((elem, index) =>
                            <li key={ index } className="list-group-item list-group-item-action flex-column align-items-start" onClick={ () => this.props.onSuggestionSelected(elem) } >
                                { React.createElement(this.props.suggestionComponent, { value: elem }, null) }
                            </li>
                        )}
                    </ul>
                </div>
            </React.Fragment>
        );
    }
    
    static defaultProps = {
        value: '',
        error: '',
        message: '',
        suggestions: [],
        onChange: () => { return; },
        onSuggestionSelected: () => { return; },
        suggestionComponent: DefaultSuggestionComponent,
    }

    static propTypes = {
        value: PropTypes.string,
        error: PropTypes.string,
        message: PropTypes.string,
        suggestions: PropTypes.array,
        onChange: PropTypes.func,
        onSuggestionSelected: PropTypes.func,
    }
}

export default Autocomplete