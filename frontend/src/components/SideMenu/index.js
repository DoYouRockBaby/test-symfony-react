import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import type { Dispatch } from 'redux'
import type { ReducerState } from '../../reducers'
import type { Thread, Section, Area } from '../../types/Entity';
import { displaySectionCreation, hideSectionCreation, titleChange, createSection } from '../../actions/section'
import { createThread } from '../../actions/thread'
import PropTypes from 'prop-types'
import './index.css'

type Props = {
    area: ?Area,
    isCreatingSection: boolean,
    title: string,
    onDisplaySectionCreation: () => void,
    onHideSectionCreation: () => void,
    onTitleChange: (value: string) => void,
    onSectionCreated: (area: Area) => void,
    onThreadCreated: (thread: Thread, section: Section, area: Area) => void,
};

class SideMenu extends Component<Props> {
    onTitleChange = (ev: Event) => {
        if (ev.target instanceof HTMLInputElement) {
            if(this.props.onTitleChange !== null) {
                this.props.onTitleChange(ev.target.value)
            }
        }
    }

    onHideSectionCreation = (ev: Event) => {
        ev.preventDefault()
        this.props.onHideSectionCreation()
    }

    onSectionCreated = (ev: Event) => {
        ev.preventDefault()

        this.props.onSectionCreated({
            title: this.props.title,
            area: this.props.area,
        })
    }

    render() {
        return (
            <nav className="sidebar">
                { this.props.area && this.props.area.sections && this.props.area.sections.map((section: Section) =>
                    <div key={ section.id } >
                        <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1" >
                            <span>{ section.title }</span>
                            <button className="plus-circle" />
                        </h6>

                        <ul className="nav flex-column mb-2" >
                            { section.threads && section.threads.map((thread: Thread) =>
                                <li key={ thread.id } className="nav-item" >
                                    <a className="nav-link" href={null} >{ thread.title }</a>
                                </li>
                            )}
                        </ul>
                    </div>
                )}
                
                <div className={ "p-1" + ((this.props.area !== null && !this.props.isCreatingSection) ? "" : " d-none") } >
                    <button className="btn btn-outline-success container-fluid" type="submit" onClick={ this.props.onDisplaySectionCreation } >Add</button>
                </div>
                
                <form className={ "container" + ((this.props.area !== null && this.props.isCreatingSection) ? "" : " d-none") } onSubmit={ this.onSectionCreated } >
                    <div className="row p-1" >
                        <input className="form-control" type="text" placeholder="Title" onChange={ this.props.onTitleChange } />
                    </div>
                    <div className="row" >
                        <div className="p-1 col-6" >
                            <button className="btn btn-outline-success container-fluid" type="submit" >Add</button>
                        </div>
                        
                        <div className="p-1 col-6" >
                            <button className="btn btn-outline-danger container-fluid" type="submit" onClick={ this.onHideSectionCreation } >Cancel</button>
                        </div>
                    </div>
                </form>
            </nav>
        );
    }
    
    static defaultProps = {
        area: null,
        isCreatingSection: false,
        onDisplaySectionCreation: () => { return; },
        onHideSectionCreation: () => { return; },
        title: '',
        onSectionCreated: () => { return; },
        onThreadCreated: () => { return; },
    }

    static propTypes = {
        area: PropTypes.object,
        onDisplaySectionCreation: PropTypes.func,
        onHideSectionCreation: PropTypes.func,
        title: PropTypes.string,
        onSectionCreated: PropTypes.func,
        onThreadCreated: PropTypes.func,
    }
}

const mapStateToProps = (state: ReducerState) => {
    return {
        area: state.area.area,
        isCreatingSection: state.section.isCreating,
        title: state.section.title,
    }
}

const mapDisplatchToProps = (dispatch: Dispatch<any>) => bindActionCreators({
    onDisplaySectionCreation: () => displaySectionCreation(),
    onHideSectionCreation: () => hideSectionCreation(),
    onTitleChange: (ev) => titleChange(ev.target.value),
    onSectionCreated: (area) => createSection(area),
    onThreadCreated: (thread, section, area) => createThread(thread, section, area),
}, dispatch)

export default connect(mapStateToProps, mapDisplatchToProps)(SideMenu)