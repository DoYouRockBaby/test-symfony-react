import React, { Component } from 'react'
import PropTypes from 'prop-types'
import type { Area } from '../../types/Entity'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import type { Dispatch } from 'redux'
import type { ReducerState } from '../../reducers'
import { selectArea, createArea, titleChange, areaDisplayCreation, areaDisplaySelection } from '../../actions/area'
import './index.css'

type Props = {
    title: string,
    areas: Array<Area>,
    selectedArea: ?Area,
    isCreating: boolean,
    onTitleChange: (value: string) => void,
    onAreaSelected: (area: ?Area) => void,
    onAreaCreated: (area: Area) => void,
    onAreaDisplayCreation: () => void,
    onAreaDisplaySelection: () => void,
};

class AreaSelector extends Component<Props> {
    onTitleChange = (ev: Event) => {
        if (ev.target instanceof HTMLInputElement) {
            if(this.props.onTitleChange !== null) {
                this.props.onTitleChange(ev.target.value)
            }
        }
    }

    onChange = (ev: Event) => {
    }

    onAreaSelected = (ev: Event) => {
        if (ev.target instanceof HTMLSelectElement) {
            if(this.props.onAreaDisplayCreation !== null && typeof ev.target.value !== 'undefined' && ev.target.value !== null && ev.target.value !== '') {
                this.props.areas.filter((area: Area) => area.id === parseInt(ev.target.value)).forEach(area => {
                    this.props.onAreaSelected(area)
                })
            }
        }
    }

    onAreaCreated = (ev: Event) => {
        ev.preventDefault()

        this.props.onAreaCreated({
            title: this.props.title,
        })
    }

    onAreaDisplaySelection = (ev: Event) => {
        ev.preventDefault()
        this.props.onAreaDisplaySelection()
    }

    render() {
        return (
            <div className="align-self-end py-1" >
                <div className={ "area-selector form-inline" + (this.props.isCreating ? " d-none" : "") } >
                    <select className="form-control mr-sm-2" value={ (typeof this.props.selectedArea !== 'undefined' && this.props.selectedArea !== null ) ? this.props.selectedArea.id : "" } onChange={ this.onAreaSelected } >
                        <option value="" disabled hidden>{(this.props.areas.length === 0) ? "No area available" : "Choose the area"}</option>
                        { this.props.areas.map((area: Area) => 
                            <option key={ area.id } value={ area.id } >{ area.title }</option>
                        )}
                    </select>
                    <button className="form-control btn btn-outline-success mx-2 my-sm-0" type="submit" onClick={ this.props.onAreaDisplayCreation } >Add</button>
                </div>

                <form className={ "area-selector form-inline" + (this.props.isCreating ? "" : " d-none") } onSubmit={ this.onAreaCreated } >
                    <input className="form-control mr-sm-2" type="text" placeholder="Title" onChange={ this.props.onTitleChange } />
                    <button className="btn btn-outline-success mx-2 my-sm-0" type="submit" >Add</button>
                    <button className="btn btn-outline-danger mx-2 my-sm-0" type="submit" onClick={ this.onAreaDisplaySelection } >Cancel</button>
                </form>
            </div>
        );
    }
    
    static defaultProps = {
        title: '',
        areas: [],
        selectedArea: null,
        isCreating: false,
        onTitleChange: () => { return; },
        onAreaSelected: () => { return; },
        onAreaCreated: () => { return; },
        onAreaDisplayCreation: () => { return; },
        onAreaDisplaySelection: () => { return; },
    }

    static propTypes = {
        title: PropTypes.string,
        areas: PropTypes.any,
        selectedArea: PropTypes.any,
        isCreating: PropTypes.bool,
        onTitleChange: PropTypes.func,
        onAreaSelected: PropTypes.func,
        onAreaCreated: PropTypes.func,
        onAreaDisplayCreation: PropTypes.func,
        onAreaDisplaySelection: PropTypes.func,
    }
}

const mapStateToProps = (state: ReducerState) => {
    return {
        title: state.area.title,
        areas: (typeof state.user.user !== 'undefined' && state.user.user !== null) ? state.user.user.areas : [],
        selectedArea: state.area.area,
        isCreating: state.area.isCreating,
    }
}

const mapDisplatchToProps = (dispatch: Dispatch<any>) => bindActionCreators({
    onTitleChange: (ev) => titleChange(ev.target.value),
    onAreaSelected: (area) => selectArea(area),
    onAreaCreated: (area) => createArea(area),
    onAreaDisplayCreation: () => areaDisplayCreation(),
    onAreaDisplaySelection: () => areaDisplaySelection(),
}, dispatch)

export default connect(mapStateToProps, mapDisplatchToProps)(AreaSelector)