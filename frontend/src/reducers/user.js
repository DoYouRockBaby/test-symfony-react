import type { User } from '../types/Entity';

export type State = {
    user: ?User,
}

type Action = {
    type: string,
    user: ?User,
};

const initialState = {
    user: null,
};

const user = (state: State = initialState, action: Action) => {
    switch (action.type) {
        case 'REGISTER_SUCCESS':
            return Object.assign({}, state, {
                user: action.user
            })
        case 'LOGIN_SUCCESS':
            return Object.assign({}, state, {
                user: action.user
            })
        case 'USER_UPDATED':
            return Object.assign({}, state, {
                user: action.user
            })
        default:
            return state;
    }
}

export default user;