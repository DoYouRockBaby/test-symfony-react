import type { ErrorMessage } from '../types/ErrorMessage'
import { isArray } from 'util'
import type { Area } from '../types/Entity'

export type State = {
    request: string,
    error: string;
    areas: Array<Area>,
}

type Action = {
    type: string,

    request: string,
    areas: Array<Area>,
    errors: Array<ErrorMessage>,
};

const initialState = {
    request: '',
    error: '',
    areas: [],
};

const loginRegister = (state: State = initialState, action: Action) => {
    switch (action.type) {
        case 'MAIN_SEARCH_REQUEST_CHANGE':
            return Object.assign({}, state, {
                request: action.request,
            })
        case 'MAIN_SEARCH_REQUEST':
            return Object.assign({}, state, {
                areas: [],
            })
        case 'AREA_SELECTED':
            return Object.assign({}, state, {
                request: '',
                areas: [],
            })
        case 'MAIN_SEARCH_SUCCESS':
            return Object.assign({}, state, {
                areas: action.areas,
                error: (isArray(action.areas) && action.areas.length === 0) ? 'No search result' : '',
            })
        case 'MAIN_SEARCH_ERROR':
            return Object.assign({}, state, {
                error: 'Error during search query',
            })
        default:
            return state;
    }
}

export default loginRegister;