import type { Area, Section } from '../types/Entity';

export type State = {
    area: ?Area,
    title: string,
    isCreating: boolean,
}

type Action = {
    type: string,
    area: ?Area,
    section: ?Section,
    title: string,
    isCreating: boolean,
};

const initialState = {
    area: null,
    title: '',
    isCreating: false,
};

const user = (state: State = initialState, action: Action) => {
    switch (action.type) {
        case 'AREA_SELECTED':
            return Object.assign({}, state, {
                area: action.area
            })
        case 'AREA_TITLE_CHANGE':
            return Object.assign({}, state, {
                title: action.title
            })
        case 'AREA_CREATION_DISPLAY':
            return Object.assign({}, state, {
                isCreating: true
            })
        case 'AREA_SELECTION_DISPLAY':
            return Object.assign({}, state, {
                isCreating: false
            })
        case 'SECTION_CREATED':
            let area = state.area

            if(typeof area !== 'undefined' && area !== null && typeof action.section !== 'undefined' && action.section !== null) {
                if(typeof area.sections !== 'undefined' && area.sections !== null) {
                    area.sections.push(action.section)
                } else {
                    area.sections = [ action.section ]
                }
            }

            return Object.assign({}, state, {
                area: area
            })
        default:
            return state;
    }
}

export default user;