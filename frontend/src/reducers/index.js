import { combineReducers } from 'redux'
import area from './area'
import user from './user'
import loginRegister from './loginRegister'
import mainSearch from './mainSearch'
import thread from './thread'
import section from './section'

//Create reducers array
const reducers = {
    area,
    user,
    loginRegister,
    mainSearch,
    thread,
    section,
};

//Generate reducers type
type $ExtractFunctionReturn = <V>(v: (...args: any) => V) => V;
export type ReducerState = $ObjMap<typeof reducers, $ExtractFunctionReturn>;

//Combine reducers
export default combineReducers(reducers)