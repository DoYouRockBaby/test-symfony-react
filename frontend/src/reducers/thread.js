import type { Thread, Message } from '../types/Entity';

export type State = {
    thread: ?Thread,
    message: string,
    messages: Array<Message>,
}

type Action = {
    type: string,
    thread: ?Thread,
    message: string,
    messages: Array<Message>,
};

const initialState = {
    thread: null,
    message: '',
    messages: [
        {
            id: 1,
            message: "test 1",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 2,
            message: "test 2",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 3,
            message: "test 3",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 4,
            message: "test 4",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 5,
            message: "test 5",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 6,
            message: "test 6",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 7,
            message: "test 7",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 8,
            message: "test 8",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 9,
            message: "test 9",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 10,
            message: "test 10",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 11,
            message: "test 11",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 12,
            message: "test 12",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 13,
            message: "test 13",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 14,
            message: "test 14",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 15,
            message: "test 15",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 16,
            message: "test 16",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 17,
            message: "test 17",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 18,
            message: "test 18",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 19,
            message: "test 19",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
        {
            id: 20,
            message: "test 20",
            datetime: new Date(),
            thread: {
                id: 1,
                title: 'test'
            },
        },
    ],
};

const thread = (state: State = initialState, action: Action) => {
    switch (action.type) {
        case 'MESSAGE_CHANGE':
            return Object.assign({}, state, {
                message: action.message,
            })
        case 'THREAD_CREATED':
            let section = state.area.sections.filter((section: Section) => section.id == action.thread.section.id)[0];

            if(typeof section !== 'undefined' && section !== null && typeof action.thread !== 'undefined' && action.thread !== null) {
                if(typeof section.threads !== 'undefined' && section.threads !== null) {
                    section.threads.push(action.thread)
                } else {
                    section.threads = [ action.thread ]
                }
            }

            return Object.assign({}, state, {
                area: area
            })
        default:
            return state;
    }
}

export default thread;