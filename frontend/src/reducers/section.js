import type { Area } from '../types/Entity';

export type State = {
    title: string,
    isCreating: boolean,
}

type Action = {
    type: string,
    title: string,
    isCreating: boolean,
};

const initialState = {
    title: '',
    isCreating: false,
};

const user = (state: State = initialState, action: Action) => {
    switch (action.type) {
        case 'SECTION_TITLE_CHANGE':
            return Object.assign({}, state, {
                title: action.title
            })
        case 'SECTION_CREATED':
            return Object.assign({}, state, {
                title: ''
            })
        case 'SECTION_CREATION_DISPLAY':
            return Object.assign({}, state, {
                isCreating: true
            })
        case 'SECTION_CREATION_HIDE':
            return Object.assign({}, state, {
                isCreating: false
            })
        default:
            return state;
    }
}

export default user;