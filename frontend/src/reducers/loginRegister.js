import { LoginRegisterDisplayStateEnum } from '../types/Enum'
import type { ErrorMessage } from '../types/ErrorMessage';
import { messageFromProperty } from '../utilities/ErrorMessageUtility'

export type State = {
    action: string,
    username: string,
    password: string,
    confirmPassword: string,
    usernameError: string,
    passwordError: string,
    confirmPasswordError: string,
}

type Action = {
    type: string,

    errors: Array<ErrorMessage>,
    username: string,
    password: string;
    confirmPassword: string;
};

const initialState = {
    action: LoginRegisterDisplayStateEnum.LOGIN,
    
    username: '',
    password: '',
    confirmPassword: '',
    usernameError: '',
    passwordError: '',
    confirmPasswordError: '',
};

const loginRegister = (state: State = initialState, action: Action) => {
    switch (action.type) {
        case 'USERNAME_CHANGE':
            return Object.assign({}, state, {
                username: action.username,
                usernameError: '',
            })
        case 'PASSWORD_CHANGE':
            return Object.assign({}, state, {
                password: action.password,
                passwordError: '',
                confirmPasswordError: (action.password !== state.confirmPassword) ? 'Password don\'t match' : '',
            })
        case 'CONFIRM_PASSWORD_CHANGE':
            return Object.assign({}, state, {
                confirmPassword: action.confirmPassword,
                confirmPasswordError: (state.password !== action.confirmPassword) ? 'Password don\'t match' : '',
            })
        case 'REGISTER_REQUEST':
            return state;
        case 'REGISTER_ERROR':
            return Object.assign({}, state, {
                usernameError: messageFromProperty('username', action.errors),
                passwordError: messageFromProperty('password', action.errors),
            })
        case 'LOGIN_REQUEST':
            return state;
        case 'LOGIN_ERROR':
            return Object.assign({}, state, {
                usernameError: messageFromProperty('username', action.errors),
                passwordError: messageFromProperty('password', action.errors),
            })
        case 'DISPLAY_LOGIN':
            return Object.assign({}, state, {
                action: LoginRegisterDisplayStateEnum.LOGIN,
                username: '',
                password: '',
                confirmPassword: '',
                usernameError: '',
                passwordError: '',
                confirmPasswordError: '',
            })
        case 'DISPLAY_REGISTER':
            return Object.assign({}, state, {
                action: LoginRegisterDisplayStateEnum.REGISTER,
                username: '',
                password: '',
                confirmPassword: '',
                usernameError: '',
                passwordError: '',
                confirmPasswordError: '',
            })
        default:
            return state;
    }
}

export default loginRegister;