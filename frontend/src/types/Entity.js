export type Area = {
    id: ?number,
    title: ?string,
    admins: ?Array<User>,
    users: ?Array<User>,
    sections: ?Array<Section>,
};

export type Message = {
    id: ?number,
    message: ?string,
    datetime: ?Date,
    thread: ?Thread,
};

export type Section  = {
    id: ?number,
    title: ?string,
    area: ?Area,
    threads: ?Array<Thread>,
};

export type Thread = {
    id: ?number,
    title: ?string,
    section: ?Section,
};

export type User = {
    id: ?number,
    username: ?string,
    areas: ?Array<Area>,
};