export const LoginRegisterDisplayStateEnum = {
    CHAT: 'CHAT',
    LOGIN: 'LOGIN',
    REGISTER: 'REGISTER',
}